export class Paciente { // export para que otras clases la puedan leer
    idPaciente: number;
    nombres: string;
    apellidos: string;
    dpi: string;
    direccion: string;
    telefono: string;
}
