import { Paciente } from './paciente';

export class Signos { // export para que otras clases la puedan leer
    idSignos: number;
    paciente: Paciente;
    fecha: string;
    temperatura: string;
    pulso: string;
    ritmo: string;
}
