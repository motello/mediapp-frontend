import { PacienteNuevoAhoraComponent } from './pages/signos/signos-edicion/paciente-nuevo-ahora/paciente-nuevo-ahora.component';
import { SignosEdicionComponent } from './pages/signos/signos-edicion/signos-edicion.component';
import { SignosComponent } from './pages/signos/signos.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { ExamenComponent } from './pages/examen/examen.component';

const routes: Routes = [
  // para que se pueda navegar y tenga algunos componentes para navegar es children
  { path: 'paciente', component: PacienteComponent, children: [
// rutas para navegar
// pacienteEdicionComponent: para insertar y actualizar
{ path: 'nuevo', component: PacienteEdicionComponent}, // el mismo componente pero la logica sera un poco distinta
{ path: 'edicion/:id', component: PacienteEdicionComponent}, // el dos puntos, parte dinamica para seleccionar uno en especifico
  ]},

  { path: 'examen', component: ExamenComponent, children: [
    { path: 'nuevo', component: ExamenEdicionComponent}, // el mismo componente pero la logica sera un poco distinta
    { path: 'edicion/:id', component: ExamenEdicionComponent}, // el dos puntos, parte dinamica para seleccionar uno en especifico
      ]},
      { path: 'especialidad', component: EspecialidadComponent, children: [
        { path: 'nuevo', component: EspecialidadEdicionComponent}, // el mismo componente pero la logica sera un poco distinta
        { path: 'edicion/:id', component: EspecialidadEdicionComponent}, // el dos puntos, parte dinamica para seleccionar uno en especifico
          ]},
      {path: 'signos', component: SignosComponent , children: [
        { path: 'nuevo', component: SignosEdicionComponent}, // el mismo componente pero la logica sera un poco distinta
        { path: 'edicion/:id', component: SignosEdicionComponent},
    { path: 'nuevopaciente', component: PacienteNuevoAhoraComponent}, // el dos puntos, parte dinamica para seleccionar uno en especifico
          ]},
          {path: 'medico', component: MedicoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
