import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Paciente } from '../_model/paciente';
import { Subject } from 'rxjs'; // es una bilbioteca de reacciones contra eventos de progra reactiva

// este service sirve para crear alguna logica para llamar a los servicios de Spring
@Injectable({ // para que se pueda utilizar en otras clases - una inyeccion de dependencias - esto lo hace por defecto - se crea con comando
  providedIn: 'root'
})
export class PacienteService {

  // crearemos una variable reactiva para poder manejar el caso de mandar refrescamiento al componente cuando se actualice un paciente
  // estoy diciendo: quiero una variable reactiva y dentro de el almacenamos un arreglo de pacintes
  // luego vamos a paciente edicion (componente hijo y llenar esa data alla)
  pacienteCambio = new Subject<Paciente[]>(); 
  // mensaje de snackbar y usamos variable reactiva que seria la siguiente luego ir apaciente-edicion.component.ts e implementarlo alla con:
  // en this.pacienteService.mensajeCambio.next('Se modifico');

  mensajeCambio = new Subject<string>();

  // las urls no deben de estar fijas, esa seria una mala practica, entonces creamos una carpeta
  // para poder consumir el servicio rest en Spring // host esta en var.constant.ts
  // y para que sea mas dinamico sino con el tiempo si cambian las urls se deberian refactorizar todas los service
  url: string = `${HOST}/pacientes`;


  // necesitamos hacer peticiones HTTP - para eso se necesita hablitar
  // el comportamiento HTTP en nustro proyecto (en app.module general del proyecto)
    // los 3 tipos de inyeccion de dependencia son: por metodo , por constructor y por contendedor de interfaz

  constructor(private http: HttpClient) { }  // para aplicar la inyeccion de dependencias pasamos un parametro en el constructor
  // lo que hacemos es usar lo que hemos utilizando lo que ha sido configurado en el app.module.ts y tener una instancia de HTTPCliente 
  // como un Autowired

  listar() { // ahora podemos tener el metodo listar ya que hemos hecho la inyeccion
// aqui hacemos una peticino get un arreglo de pacientes, paso los parametros que es la url a la que necesito 
//  solicitar el servicio la que hice antes y como ya se que viene por eso hice el module, automatico hace el JSON
// ahora este metodo listar necesito llamarlo desde algun otro lado
    return this.http.get<Paciente[]>(this.url);
  }

  listarPacientePorID(id: number) {
    // return this.http.get<Paciente>(this.url + '/' + id); // mala practica viejita
    // asi se concatena en la actualidad
    // ahora se concatena de la siguiente manera no con signo mas
    return this.http.get<Paciente>(`${this.url}/${id}`);
  }

  registrar(paciente: Paciente) {
    return this.http.post(this.url, paciente); // paciente es el body lo que hacia en postman - lo transforma a json
  }

  modificar(paciente: Paciente) {
    return this.http.put(this.url, paciente);
  }

  eliminar(id: number) {
return this.http.delete(`${this.url}/${id}`);
  }

}
