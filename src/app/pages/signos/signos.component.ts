import { SignosService } from './../../_service/signos.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Signos } from '../../_model/signos';
import { MatPaginator, MatSort, MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  dataSource: MatTableDataSource <Signos>; // Datasource es un orgigen de datos . . 
  // de Tipo paciente, hay que darle estructura del JSON (para eso esta el model paciente.ts que se creo en model)
  
  // el siguiente es un arreglo, debe ser lo mismo que he escrito aca alla en el html de dataTable en el mismo orden
  displayedColumns = [ 'idSignos', 'fecha', 'pulso', 'ritmo', 'temperatura', 'idPaciente', 'acciones' ];
// para enlazar el paginator y el sort al html 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private signosService: SignosService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.signosService.signosCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  this.signosService.listar().subscribe(data => {
  this.dataSource = new MatTableDataSource(data);
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
   });

   this.signosService.mensajeCambio.subscribe(data => {
    this.snackBar.open(data, 'Aviso', {
      duration: 2000,
    });
  });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idSignos: number) {
    this.signosService.eliminar(idSignos).subscribe(data => {
      this.signosService.listar().subscribe(data => {
        this.signosService.signosCambio.next(data);
        this.signosService.mensajeCambio.next('Se eliminó');
      });
    });
  }

}
