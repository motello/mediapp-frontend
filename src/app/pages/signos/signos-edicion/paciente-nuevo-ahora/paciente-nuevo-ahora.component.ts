import { SignosService } from './../../../../_service/signos.service';
import { Component, OnInit } from '@angular/core';
import { Paciente } from '../../../../_model/paciente';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PacienteService } from '../../../../_service/paciente.service';

@Component({
  selector: 'app-paciente-nuevo-ahora',
  templateUrl: './paciente-nuevo-ahora.component.html',
  styleUrls: ['./paciente-nuevo-ahora.component.css']
})
export class PacienteNuevoAhoraComponent implements OnInit {

  edicion = false; // para manejar lo del formulario en blanco 
  id: number;
  paciente: Paciente;
  // variable que funciona en el paciente-edicion.component.html
  form: FormGroup; // Es de tipo FormGroup porque necesito representar mi formulario de html aca en el typescript
  // necesitamos utilizar la clase (route) que gestiona la url o las direcciones en angular
  // la variable rout me va a permitir manipular el contenido de lo que viene en la direccion
  constructor(private signosService: SignosService, private rout: ActivatedRoute, 
    private router: Router, private pacienteService: PacienteService)
  { // inyectamos PacienteService a esta capa edicion
    this.form = new FormGroup({
      // aqui crearemos la representacion del html pero en typescript y se crea un formulario en blanco apenas carga la pagina
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dpi': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl('')

    });
  }


// apenas cargue la pagina captura el id que te estan mandando por la url
  ngOnInit() {

    this.paciente = new Paciente();
    // de este route quiero extraer sus parametros
    // realmente lo que nos interesa es obtener el id para poder llenar automaticamente el formulario
    // entonces capturamos el id a continuacion con una programacion reactiva
    this.rout.params.subscribe((params: Params) => {
    this.id = params['id']; // la palabra id sale del path en routing
    this.edicion = params['id'] != null; // si viene el id es true sino viene el id la edicion es false
    this.initForm();

    }); // como ya se que es paramtero, digo a esos parametros
  }

  initForm() { // con esto iniciamos el formulario
// la logica es: que cuando en la url venga /nuevo debe estar en blanco pero si viene algun valor el formulario
// debene tener datos de base de datos
if (this.edicion) { // si viene una url con /edicion o /numero etonces se pobla 
// cargar la data del servicio en el formulario
this.pacienteService.listarPacientePorID(this.id).subscribe(data => { // le pasamos el id que viene en la url que es.... 
// ... la de this.id = params['id']; esta arriba
  this.form = new FormGroup({
  'id': new FormControl(data.idPaciente),
  'nombres': new FormControl(data.nombres),
  'apellidos': new FormControl(data.apellidos),
  'dpi': new FormControl(data.dpi),
  'direccion': new FormControl(data.direccion),
  'telefono': new FormControl(data.telefono)

});



}); // y quiero extraer la data con el suscribe es decir vaciarla al formulario
} // else {
// else devolvemos el forumario vacio


// }

  }

  operar() { // para trabajar el area de nuevo
// primero necesito extraer la info que estoy capturando en el html
// necesito mandar un objeto paciente

// la siguiente linea dice: este paciente su id viene a ser lo que esta en el formlario del campo cuyo identificador es

this.paciente.idPaciente = this.form.value['id'];
this.paciente.nombres = this.form.value['nombres'];
this.paciente.apellidos = this.form.value['apellidos'];
this.paciente.dpi = this.form.value['dpi'];
this.paciente.direccion = this.form.value['direccion'];
this.paciente.telefono = this.form.value['telefono'];

if (this.edicion) {
// actualizar
this.pacienteService.modificar(this.paciente).subscribe(data => {
// ahora con lo que hice en paciente.service.ts hare esto para actualizar
this.pacienteService.listar().subscribe(pacientes => { // todos los pacientes que vienen de la base de datos
  // utilizamos la variable reactiv que esta en service, en su funcion next podemos almacenar la informacion nueva que contiene el cambio
  // ahor l component principl pacien deb recepcionar est cambi q sucedio n paciene edicion,esto se hara en el ngOninit de componente padre
this.pacienteService.pacienteCambio.next(pacientes); 
// esta es la variable reactiva que viene de paciente.service.ts y luego de aca nos vamos a paciente.component.ts para reaccionar a cambios
this.pacienteService.mensajeCambio.next('Se modifico'); 
});
});
} else {
  // registrar
  this.pacienteService.registrar(this.paciente).subscribe(data => {
    this.pacienteService.listar().subscribe(pacientes => { // todos los pacientes que vienen de la base de datos
// utilizamos la variable reactiv que esta en service, en su funcion next podemos almacenar la informacion nueva que contiene el cambio
// ahor l component principl pacien deb recepcionar est cambi q sucedio n paciene edicion,esto se hara en el ngOninit de componente padre
this.pacienteService.pacienteCambio.next(pacientes);
this.signosService.pacienteNuevo.next(pacientes);
this.pacienteService.mensajeCambio.next('Se registro');

  });
});
  }



// Cuando se cierre lo de nuevo o edicion que se vaya a el componente principal, por medio de router: Router
// por medio de este router vamos a navegar hacia una definicion qeu este en el app.routing en este caso paciente
this.router.navigate(['/signos/nuevo']);

  }

}
