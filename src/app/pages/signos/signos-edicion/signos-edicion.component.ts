import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { SignosService } from './../../../_service/signos.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Signos } from './../../../_model/signos';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  id: number;
  signos: Signos;
  form: FormGroup;
  edicion: boolean =  false;

  myControlPaciente: FormControl = new FormControl();

  pacientes: Paciente[] = [];
  filteredOptions: Observable<any[]>;
  pacienteSeleccionado: Paciente;




  constructor(private pacienteService: PacienteService, private builder: FormBuilder,
    private signosService: SignosService, private route: ActivatedRoute, private router: Router) {
    this.signos = new Signos();

    this.form = builder.group({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl('')
    });

  }

  ngOnInit() {
    this.listarPacientes();
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
   // this.pacienteSeleccionado = this.pacienteService.pacienteCambio;
     this.signos = new Signos();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

  // para poder ingresar el paciente luego de ingresarlo como nuevo
this.signosService.pacienteNuevo.subscribe(data => {
  console.log(data);
  // this.myControlPaciente.setValue('paciente');
  // const paciente = data.paciente;
        this.form = new FormGroup({
            'paciente': new FormControl(this.myControlPaciente)
          });
  });


  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) ||
option.dpi.includes(val.dpi));
    } else {
      return this.pacientes.filter(option =>
option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) ||
option.dpi.includes(val));
    }
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
    this.pacientes = data;
    console.log(data);

// para poder ingresar el paciente luego de ingresarlo como nuevo
this.signosService.pacienteNuevo.subscribe(dat => {
  console.log(dat);
  // this.myControlPaciente.setValue('paciente');
  // const paciente = data.paciente;
  this.pacientes = dat;
     //   this.form = new FormGroup({
       //     'paciente': new FormControl(dat)
       //   });
  });


    });
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  initForm() {
    if (this.edicion) {
      this.signosService.listarSignosPorId(this.id).subscribe(data => {
        const id = data.idSignos;
        const fecha = data.fecha;
        const pulso = data.pulso;
        const ritmo = data.ritmo;
        const temperatura = data.temperatura;
        const paciente = data.paciente;
        this.form = new FormGroup({
          'id': new FormControl(id),
          'paciente': new FormControl(paciente),
          'fecha': new FormControl(fecha),
          'pulso': new FormControl(pulso),
          'ritmo': new FormControl(ritmo),
          'temperatura': new FormControl(temperatura),
        });
      });
    }
  }

  operar() {

    this.signos.idSignos = this.form.value['id'];
    // this.signos.paciente = this.pacienteSeleccionado;
      this.signos.paciente = this.form.value['paciente'];
    this.signos.fecha = this.form.value['fecha'];
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.ritmo = this.form.value['ritmo'];


    this.signosService.listarSignosPorId(this.id).subscribe(data => {
      console.log(data);
    });

    if (this.signos != null && this.signos.idSignos > 0) {


      this.signosService.modificar(this.signos).subscribe(data => {
        this.signosService.listar().subscribe(signos => {
          console.log(data);
          this.signosService.signosCambio.next(signos);
          this.signosService.mensajeCambio.next('Se modifico');
        });
      });
         /*
         this.signosService.listarSignosPorId(this.id).subscribe(data => {
          console.log(data);
        });
*/

    } else {
      this.signosService.registrar(this.signos).subscribe(data => {
         this.signosService.listar().subscribe(signos => {
          console.log(data);
          this.signosService.signosCambio.next(signos);
           this.signosService.mensajeCambio.next('Se registro');
         });
      });
    }

    this.router.navigate(['signos']);
  }

  seleccionarPaciente(e: any) {
  console.log(e);
  this.pacienteSeleccionado = e.option.value;
  console.log(e);
  }

  eliminar(idSignos: number) {
    this.signosService.eliminar(idSignos).subscribe(data => {
      this.signosService.listar().subscribe(data => {
        this.signosService.signosCambio.next(data);
      this.signosService.mensajeCambio.next('Se elimino');
    });
    });
      }




}
