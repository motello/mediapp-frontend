import { Paciente } from './../../_model/paciente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { PacienteService } from '../../_service/paciente.service';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit { // ddddd
  dataSource: MatTableDataSource <Paciente>; // Datasource es un orgigen de datos . . 
  // de Tipo paciente, hay que darle estructura del JSON (para eso esta el model paciente.ts que se creo en model)
  
  // el siguiente es un arreglo, debe ser lo mismo que he escrito aca alla en el html de dataTable en el mismo orden
  displayedColumns = [ 'idPaciente', 'nombres', 'apellidos', 'acciones' ];
// para enlazar el paginator y el sort al html 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  // no se pone el observable en el constructor porque debe estar la instancia creada antes para poder usarla
  constructor(private pacienteService: PacienteService, private snackBar: MatSnackBar ) { } // hacemos una inyeccion de servicePaciente
// apenas cargue hara estodebemos poblarel datasource conla data que viene del servic
  ngOnInit() { // ahora necesitamos traer la data para poblar la tabla es para lo que sirve lo siguiente  
    // aplicamos la variable reactiva para volver a mostrar la data (esto que va dentro se copio del de abajo)
    this.pacienteService.pacienteCambio.subscribe(data => {// se llama cada vez que el subcompoente hace un cambio


      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    // snackbar
    this.pacienteService.mensajeCambio.subscribe(data => {// obtengo la data que seria el mensaje
      // tengo que inyectar el snackbar para poderlo usar a continuacion con un mensaje emergente
  this.snackBar.open(data, 'Aviso', {duration: 2000});
    });

    // a continuacion se aplica la programacion reactiva (lo que hce es leer constantemente un stram de datos..
    //  y programar bajo lo que suceda en ese strem de datos) = frente a determinados eventos reacciona este tipo de progra
    // con los observables manipulamos dentro de la llamada con operadores .map  es como una tuberia de datos
    // cuando sucede un evento el objeto que esta en estudio que es el objeto observado notifica a sus suscriptores que ha pasado algo
    // entonces necesitamos a continucacion suscribirnos a un observable para poder leer el contenido de la respuesta que pase en este obsr
    this.pacienteService.listar().subscribe(data => { // con esto obtengo la respuesta de lo que hay dentro de este obervable
      this.dataSource = new MatTableDataSource(data); // poblamos el datasource con la lis que viene del servicio y pasamos la data y luego 
      // ..en html definimos las columnas para mostrar el contenido de la data
      this.dataSource.paginator = this.paginator; // su paginador es lo que es amarrado del html en donde esta viewchild
      this.dataSource.sort = this.sort;
    });

  }
  applyFilter(filterValue: string) { // para filtrar nombres en la busqueda
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busco en el dataSource el metodo filter
  }

  eliminar(idPaciente: number) {
this.pacienteService.eliminar(idPaciente).subscribe(data => {
  this.pacienteService.listar().subscribe(data => {
    this.pacienteService.pacienteCambio.next(data);
  this.pacienteService.mensajeCambio.next('Se elimino');
});
});
  }
}
