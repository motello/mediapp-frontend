import { MedicoService } from './../../_service/medico.service';
import { Medico } from './../../_model/medico';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { DialogoComponent } from './dialogo/dialogo.component';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  displayedColumns = ['idMedico', 'nombres', 'apellidos', 'colegiado', 'acciones'];
  datasource: MatTableDataSource<Medico>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private medicoService: MedicoService, private dialog: MatDialog, private snackBar: MatSnackBar) { }

  ngOnInit() {
// para refrescar el datasource de medicos otra vez

this.medicoService.medicosCambio.subscribe(data => {
  this.datasource = new MatTableDataSource(data);
  this.datasource.paginator = this.paginator;
  this.datasource.sort = this.sort;
});

this.medicoService.mensaje.subscribe(data => {
  this.snackBar.open(data, 'Aviso', { duration: 2000 });
});


    this.medicoService.listar().subscribe(data => {
      this.datasource = new MatTableDataSource(data);
      // referencias al paginador
      this.datasource.paginator = this.paginator;
      this.datasource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.datasource.filter = filterValue;
  }

  openDialog(medico: Medico) {
    let med = medico != null ? medico : new Medico();
  this.dialog.open(DialogoComponent, {
    width: '250px',
    disableClose: true,
    data: med

  })
  }

  eliminar(medico: Medico) {
    this.medicoService.eliminar(medico.idMedico).subscribe( data => {
      this.medicoService.listar().subscribe(medicos => {
        this.medicoService.medicosCambio.next(medicos);
        this.medicoService.mensaje.next('Se elimino');
      });
    });
  }
}
